var app = require("express")();
var server = require("http").Server(app);
var io = require("socket.io")(server);

server.listen(6969, () => {
  console.log("Listening on 6969");
});

app.get("/user", (req, res) => {
  res.sendFile(__dirname + "/user1.html");
});

app.get("/user2", (req, res) => {
  res.sendFile(__dirname + "/user2.html");
});

io.on("connection", function(socket) {
  console.log("User connected!");
  socket.emit("ready");
  socket.on("chat", function(data) {
    socket.broadcast.emit("broadcastChat", data);
  });
});
